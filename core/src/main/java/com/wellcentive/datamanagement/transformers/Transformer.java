package com.wellcentive.datamanagement.transformers;

import lombok.Data;

import java.util.List;

//@Data
public class Transformer {

    private String name;
    private List<InboundFile> files;

}
