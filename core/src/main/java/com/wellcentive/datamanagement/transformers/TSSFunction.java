package com.wellcentive.datamanagement.transformers;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Map;

//@JsonTypeInfo(
//        use = JsonTypeInfo.Id.NAME,
//        property = "type"
//)
//@JsonSubTypes({
//        @JsonSubTypes.Type(value = EqualFunction.class, name = "eq"),
//        @JsonSubTypes.Type(value = ConcatFunction.class, name = "concat"),
//        @JsonSubTypes.Type(value = SplitFunction.class, name = "split"),
//        @JsonSubTypes.Type(value = ConditionFunction.class, name = "condition"),
//        @JsonSubTypes.Type(value = SimpleFunction.class, name = "simple")
//})
public interface TSSFunction {

    boolean accept();
    void apply(Map<String, Object> map);

}
