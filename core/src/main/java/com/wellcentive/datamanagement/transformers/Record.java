package com.wellcentive.datamanagement.transformers;

import lombok.Data;

import java.util.List;

//@Data
public class Record {

    private String type;
    private TSSFunction recordFunction;
    private List<TSSFunction> fieldFunction;

}
