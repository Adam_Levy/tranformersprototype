package com.wellcentive.datamanagement.transformers;

import lombok.Data;

import java.util.List;

//@Data
public class InboundFile {

    private String namePattern;
    private List<Record> records;

}
