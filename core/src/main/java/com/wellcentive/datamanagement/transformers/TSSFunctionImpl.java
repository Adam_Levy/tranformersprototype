package com.wellcentive.datamanagement.transformers;

import lombok.Data;

import java.util.List;
import java.util.Map;

//@Data
public class TSSFunctionImpl implements TSSFunction {

    protected List<TSSFunction> rules;
    protected List<TSSFunction> actions;

    protected Integer[] columns;
    protected String[] interfaceProperties;
    protected String[] constants;

    protected String[] destinations;

    @Override
    public boolean accept() {
        return true;
    }

    @Override
    public void apply(Map<String, Object> map) {
        //do nothing
    }

}
