package com.wellcentive.datamanagement.transformers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Collections;

public class TransformerProcessorTest {

    @Test
    public void test() throws IOException {
        Transformer transformer = new Transformer();
        transformer.setName("Test");

        InboundFile inboundFile = new InboundFile();
        inboundFile.setNamePattern("%Test*");

        Record record = new Record();
        record.setType("DEMOGRAPHICS");

        EqualFunction eqFunction = new EqualFunction();
        eqFunction.setExpected("test");
        eqFunction.setColumns(new Integer[]{1});

        record.setRecordFunction(eqFunction);
        record.setFieldFunction(Collections.singletonList(eqFunction));

        inboundFile.setRecords(Collections.singletonList(record));
        transformer.setFiles(Collections.singletonList(inboundFile));

        String json = new ObjectMapper().writeValueAsString(transformer);

        System.out.println(json);

        Transformer result = new ObjectMapper().readValue(json, Transformer.class);

        Assert.assertNotNull(result);
    }

}
